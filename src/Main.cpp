#include "array.hpp"
#include "network.hpp"
#include "crypto.hpp"

#include <string>
#include <iostream>
#include <iostream>
#include <iomanip>
#include <ostream>
#include <cstring>
#include <unistd.h>
#include <openssl/rsa.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <fstream>

using namespace std;

int main (int argc, char const **argv)
{
int fd;

	array::array *pacote_recebe;
	// PACOTE COM CONTEUDO

	array::array *pacote_enviar = array::create(7);

	pacote_enviar->data[0] = 0x03;
	pacote_enviar->data[1] = 0;
	pacote_enviar->data[2] = 0;
	pacote_enviar->data[3] = 0;
	pacote_enviar->data[4] = 0xC0;
	pacote_enviar->data[5] = 0;
	pacote_enviar->data[6] = 0;	

	cout << "Iniciando a comunicacao com o servidor" << endl;

	if ((fd = network::connect("45.55.185.4", 3000)) < 0) 
	{
		cout << "Falha ao conectar:" << fd << endl;
	}

	else 
	{
	cout << "Conexao OK: " << fd << endl;
	
	network::write(fd, pacote_enviar);
		if ((pacote_recebe = network::read(fd)) == nullptr)
		{
		cout << "Leitura NULA" << endl;
		}
		else
		{
		cout << "Imprimindo conteudo do pacote recebido: " << pacote_recebe->length << "]" << endl;
		for (int i=0; i< ((int) pacote_recebe->length); i++)
			{
			printf ("%X ", pacote_recebe->data[i]);
			if (pacote_recebe->data[i] == 0xC1)
				{
				cout << " Registration Start " << endl;
				}			
			}
		printf("\n");
		}
	}

// FUNCAO QUE LE A CHAVE PUBLICA DO SERVIDOR E CHAVE PRIVADA DO CLIENTE
 
RSA * serverkey = crypto::rsa_read_public_key_from_PEM("../server_pk.pem");
RSA * privatekey = crypto::rsa_read_private_key_from_PEM("../private.pem");

// criando array com meu id
array:: array *id_usuario = array::create(8);

id_usuario->data[0] = 0xf9;
id_usuario->data[1] = 0x9e;
id_usuario->data[2] = 0xf6;	 
id_usuario->data[3] = 0x7b;
id_usuario->data[4] = 0x0d;
id_usuario->data[5] = 0x6d;
id_usuario->data[6] = 0xd1;
id_usuario->data[7] = 0x6d;


	cout << "ID do usuario" << endl;
	
	cout << *id_usuario << endl;
	
// FUNCAO QUE ENCRIPTA O ID USANDO A CHAVE PUBLICA

array::array *id_cifrado = crypto::rsa_encrypt(id_usuario, serverkey);

//cout << "ID criptografado:  " << *id_cifrado << endl;

int conta=0, tamanho=0;
// CONTANDO O TAMANHO DO ID

for (int i=0; i<((int) id_cifrado->length); i++) {conta++;}

cout << "Tamanho do ID cifrado:   " << conta << endl;

//tamanho = tag + length + value + hash

tamanho = conta + 23;

	array::array *pacote_id = array::create(tamanho);
	
	pacote_id->data[0] = 0xC2;
   	pacote_id->data[1] = 0x00;
   	pacote_id->data[2] = 0x02;

// USANDO MENCPY (ESPACO EMBRANCO Q VAI RECEBER A COPIA, O QUE VAI SER COPIADO, TAMANHO)
	memcpy (pacote_id->data +3, id_cifrado->data, conta);

// UTILIZANDO A HASH
	array::array* hash = crypto::sha1(id_cifrado);

// COPIANDO A HASH NO PACOTE_ID
	memcpy (pacote_id->data +(3+conta), hash->data, 20);

// pacote_register e o pacote final, o que sera enviado
	array::array *pacote_register = array::create(4+tamanho);

	pacote_register->data[0] = 0x17;
	pacote_register->data[1] = 0x02;
	pacote_register->data[2] = 0;
	pacote_register->data[3] = 0;
	
	
//copiando o pacote_id dentro do pacote_register	
	memcpy(pacote_register->data +4, pacote_id->data, tamanho);
	
//	cout << "Pacote Registro = " << *pacote_register << endl;	

	array::destroy(pacote_id);
	
	//cout <<"Tamanho = "<< tamanho << endl;
	
//enviando o pacote_register para obter a chave
int c=0; 
array::array *registro;
	if (fd < 0 ) 
	{
	cout << "Erro durante a conexao" << endl;
	return 1;
	}

	else 
	{
		network::write(fd, pacote_register);
		
		if ((registro = network::read(fd)) == nullptr)
		{
		cout << "Leitura Nula" << endl;
		}
		
		else 
		{
		cout << "Imprimindo conteudo do pacote registro" << endl;
		for (int i =0; i< ((int) registro->length); i++)
			{ c++;
			//printf ("%X ", registro->data[i]); 
			}		

		cout << "Tamanho do registro = " << c << endl;
		printf("\n");
		}

	
	}
// DESCRIPTOGRAFANDO A CHAVE S 

array::array *skey;
array::array *scripto = array::create(conta);
memcpy(scripto->data, registro-> data +7, conta);

skey = crypto::rsa_decrypt(scripto, privatekey);
cout << "Chave S = " << endl;
for (int i =0; i< ((int) skey->length); i++)
			{
			printf ("%X ", skey->data[i]); 
			}	
printf ("\n");


// PEDINDO AUTENTICACAO. 1 ENVIAR PACOTE COM ID CIFRADO
array::array *pacote_auxiliar = array::create(tamanho);
array::array *pacote_autenticar;

pacote_auxiliar->data[0] = 0xA0;
pacote_auxiliar->data[1] = 0x00;
pacote_auxiliar->data[2] = 0x02;

memcpy(pacote_auxiliar->data +3, id_cifrado->data, conta);
memcpy (pacote_auxiliar->data +(3+conta), hash->data, 20);

pacote_autenticar = array::create(tamanho+4);

pacote_autenticar->data[0] = 0x17;
pacote_autenticar->data[1] = 0x02;
pacote_autenticar->data[2] = 0;
pacote_autenticar->data[3] = 0;

memcpy(pacote_autenticar->data +4, pacote_auxiliar->data, tamanho);
array::destroy(pacote_auxiliar);

//ENVIANDO O PACOTE AUTENTICAR PARA INICIAR A AUTENTICACAO (MESMO PROCESSO DE SEMPRE PARA ENVIAR UM PACOTE)

array::array *autenticado;
if (fd < 0 ) 
	{
	cout << "Erro durante a conexao" << endl;
	return 1;
	}

	else 
	{
		network::write(fd, pacote_autenticar);
		
		if ((autenticado = network::read(fd)) == nullptr)
		{
		cout << "Leitura Nula" << endl;
		}
		
		else 
		{
	/*	cout << "Imprimindo conteudo do pacote autenticar" << endl;
		cout << *autenticado << endl; */
		}
	}

array::array *tokenA;
array::array *auxilio = array::create(conta);
memcpy(auxilio->data, autenticado->data +7, conta);
tokenA = crypto::rsa_decrypt(auxilio, privatekey);

cout << "Imprimindo token A = " << *tokenA << endl;	printf ("\n");

//CRIANDO PACOTE PEDINDO DESAFIO
array::array *pacote_desafio = array::create(7);
pacote_desafio->data[0] = 0x03;
pacote_desafio->data[1] = 0;
pacote_desafio->data[2] = 0;
pacote_desafio->data[3] = 0;
pacote_desafio->data[4] = 0xA2;
pacote_desafio->data[5] = 0;
pacote_desafio->data[6] = 0;

//ENVIANDO O PACOTE DESAFIO
array::array *pacoteM;
if (fd < 0 ) 
	{
	cout << "Erro durante a conexao" << endl;
	return 1;
	}

	else 
	{
		network::write(fd, pacote_desafio);
		
		if ((pacoteM = network::read(fd)) == nullptr)
		{
		cout << "Leitura Nula" << endl;
		}
		
		else 
		{
/*		cout << "Imprimindo conteudo do pacote M" << endl;
		for (int i =0; i< ((int) pacoteM->length); i++)
			{
			printf ("%X ", pacoteM->data[i]); 
		}	*/
		printf("\n");
		}
	}
// DESCEIPTOGRAFAR O PACOTE M COM CHAVE A S E O TOKEN A, PARA OBTER O ARRAY DE DADOS M
int conta2=0, conta3=0;
for (int i=0; i<((int) pacoteM->length); i++) {conta2++;}
conta2 = conta2-27;
cout << "Tamanho do pacoteM = " << conta2 << endl;
array::array *suporte = array::create(conta2);

memcpy(suporte->data, pacoteM->data +7, conta2);

//cout << "TAMANHO Suporte  = " << *suporte << endl;
array::array *dadosM = crypto::aes_decrypt(suporte, tokenA, skey);
cout << "Dados M = " << *dadosM << endl;
for (int i=0; i<((int) dadosM->length); i++) {conta3++;} 
cout << "Tamanho e dadosM = " << conta3 << endl;

//MONTANDO PACOTE COM DADOSM
int tamanho2=0; tamanho2 = 23 + conta3;
array::array *auxiliar3 = array::create(tamanho2);

auxiliar3->data[0] = 0xA5;
auxiliar3->data[1] = 0x10;
auxiliar3->data[2] = 0x00;

memcpy(auxiliar3->data +3, dadosM->data, conta3);
array::array* hash2 = crypto::sha1(dadosM);
memcpy(auxiliar3->data+ (3+conta3), hash2->data, 20);

array::array *confere = array::create(tamanho2+4);

confere->data[0] =0x27;
confere->data[1] = 0;
confere->data[2] = 0;
confere->data[3] = 0;

memcpy(confere->data+4, auxiliar3->data, tamanho2);
array::destroy(auxiliar3);
cout << "Pacote Confere = " << *confere << endl; 

//ENVIANDO PACOTE CONFERE
array::array *lasttoken;
if (fd < 0 ) 
	{
	cout << "Erro durante a conexao" << endl;
	return 1;
	}

	else 
	{
		network::write(fd, confere);
		
		if ((lasttoken = network::read(fd)) == nullptr)
		{
		cout << "Leitura Nula" << endl;
		}
		
		else 
		{
		cout << "Imprimindo conteudo de lasttoken" << endl;
		for (int i =0; i< ((int) lasttoken->length); i++)
			{
			printf ("%X ", lasttoken->data[i]); 
		}	
		printf("\n");
		}
	}
printf("\n");
array::array *tokenTcifrado = array::create(conta2);
memcpy (tokenTcifrado->data, lasttoken->data+ 7, conta2);

array::array *tokenT = crypto::aes_decrypt(tokenTcifrado, tokenA, skey);

// ULTIMA PARTE, PEDINDO OBJETO	
  array::array *id_modelo = array::create(8);
    id_modelo->data[0] = 0x01;
    id_modelo->data[1] = 0x02;
    id_modelo->data[2] = 0x03;
    id_modelo->data[3] = 0x04;
    id_modelo->data[4] = 0x05;
    id_modelo->data[5] = 0x06;
    id_modelo->data[6] = 0x07;
    id_modelo->data[7] = 0x08;

array::array *id_crypto = crypto::aes_encrypt(id_modelo , tokenT, skey);
cout << "ID criptografado com chave S e tokenT = " << *id_crypto << endl;
int conta4=0;
for (int i=0; i < ((int) id_crypto->length); i++) { conta4++;}
cout << "Tamanho do id crifrado = " << conta4 << endl;

array::array *auxiliar4 = array::create(39);

auxiliar4->data[0] = 0xB0;
auxiliar4->data[1] = 0x10;
auxiliar4->data[2] = 0x00;

memcpy(auxiliar4->data +3, id_crypto->data, conta4);
array::array* hash3 = crypto::sha1(id_crypto);
memcpy(auxiliar4->data+ (3+conta4), hash3->data, 20);

array::array *lastpacote = array::create(43);

lastpacote->data[0] =0x27;
lastpacote->data[1] = 0;
lastpacote->data[2] = 0;
lastpacote->data[3] = 0;

memcpy(lastpacote->data+4, auxiliar4->data, 39);
array::destroy(auxiliar4);
cout << "Pacote final = " << *lastpacote << endl; 

// ENVIANDO PACOTE FINAL
array::array *objetosecreto;
if (fd < 0 ) 
	{
	cout << "Erro durante a conexao" << endl;
	return 1;
	}

	else 
	{
		network::write(fd, lastpacote);
		
		if ((objetosecreto = network::read(fd, 23143)) == nullptr)
		{
		cout << "Leitura Nula" << endl;
		}
		
		else 
		{
		cout << "Imprimindo pacote objeto" << endl;
		for (int i =0; i< ((int) objetosecreto->length); i++)
			{
			printf ("%X ", objetosecreto->data[i]); 
		}	
		printf("\n");
		}
	}

//ABRINDO O ARQUIVO DE IMAGEM OBJETO

array::array *objeto_secreto = array::create(23116);

    memcpy(objeto_secreto->data, objetosecreto->data + 7, 23116);

    array::array *objeto;

    objeto = crypto::aes_decrypt(objeto_secreto, tokenT, skey);
    
        ofstream arquivo;
	arquivo.open ("imagem.jpeg", ios::binary);        
        for(int i = 0; i < ((int) objeto->length); i++)
	{
            arquivo << objeto->data[i];
        }
    arquivo.close();

	//array::destroy(pacote_keyS);
	array::destroy(pacote_id);	
	array::destroy(id_usuario);
	array::destroy(id_cifrado);
	array::destroy(pacote_enviar);


return 0;
}
