#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>
#include <string>

class Cliente
{
private:
	string id;

public:
	Cliente();
	Cliente(string id);
	string getId();
	void setId(string id);
};
